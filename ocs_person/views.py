from django.contrib.auth.models import User
from xml.etree import ElementTree as et
from django.http import HttpResponse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate

def addUser(request):
    username = request.POST.get("login")
    password = request.POST.get("password")
    firstname = request.POST.get("firstname")
    lastname = request.POST.get("lastname")
    email = request.POST.get("email")

    status = 100
    required = [username, password, firstname, lastname, email]
    if str() in required or None in required:
        status = 101
    try:
        validate_email(email)
    except ValidationError:
        status = 106

    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    et.SubElement(meta, "status").text = "ok"
    statusfield = et.SubElement(meta, "statuscode")
    statusfield.text = str(status)
    if status is not 100:
        return HttpResponse(et.tostring(response), status=200)
    try:
        user = User.objects.create_user(username, email, password)
        user.firstname = firstname
        user.lastname = lastname
    except Exception as e:
        error = str(e)
        et.SubElement(meta, "message").text = error
        statusfield.text = "103"
    return HttpResponse(et.tostring(response), status=200)

def balance(request):
    """Gets a person's balance. This just returns zero currently."""
    response = et.Element("ocs")
    meta = et.SubElement(response, "status").text = "ok"
    et.SubElement(response, "statuscode").text = "100"
    data = et.SubElement(response, "data")
    person = et.SubElement(data, "person", details="balance")
    et.SubElement(person, "currencty").text = "Rupees"
    et.SubElement(person, "balance").text = "0.00"
    return HttpResponse(et.tostring(response), status=200)

@csrf_exempt
def checkUser(request):
    username = request.POST.get("login")
    password = request.POST.get("password")
    try:
        user = User.objects.filter(username=username)[0]
        user = user if user.check_password(password) else None
    except:
        user = None
    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    et.SubElement(meta, "status").text = "ok"
    et.SubElement(meta, "statuscode").text = "100" if user is not None else "102"
    return HttpResponse(et.tostring(response), status=200)
