from django.conf.urls import patterns, include, url
import settings
from django.views.generic import TemplateView 

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^content/categories', 'ocs_content.views.listCategories'),
    url(r'^content/data/(\d+)', 'ocs_content.views.getData'),
    url(r'^content/data', 'ocs_content.views.listItems'),
    url(r'^content/download/(\d+)/(\d+)', 'ocs_content.views.download'),
    url(r'^content/add', 'ocs_content.views.add'),
    url(r'^content/edit/(\d+)', 'ocs_content.views.edit'),
    url(r'^content/uploaddownload/(\d+)', 'ocs_content.views.uploadFile'),
    url(r'^content/licenses', 'ocs_content.views.licenses'),
    url(r'^person/add', 'ocs_person.views.addUser'),
    url(r'^person/check', 'ocs_person.views.checkUser'),
    url(r'^person/balance', 'ocs_person.views.balance'),
    url(r'^data/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^accounts/', include('registration.urls')),
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    # Examples:
    # url(r'^$', 'ocsserver.views.home', name='home'),
    # url(r'^ocsserver/', include('ocsserver.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
