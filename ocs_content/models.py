from django.db import models
from django.contrib.auth.models import User

class Category(models.Model):
    name = models.CharField(max_length=200)

class ContentEntry(models.Model):
    """This class represents one entry, e.g. "kdevelop" """
    owner = models.ForeignKey(User)
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=200)
    version = models.CharField(max_length=200)
    changed = models.DateTimeField()
    created = models.DateTimeField()
    description = models.TextField()
    summary = models.TextField()
    changelog = models.TextField()
    rating = models.FloatField(default=50)
    downloads = models.IntegerField(default=0)
    license = models.CharField(max_length=200)
    uploadedFile = models.FileField(upload_to=lambda instance, filename: "{0}/{1}".format(instance.id, filename))

# TODO this model is not even being used in a sensical way, because only one download is supported
class ContentItem(models.Model):
    """This class represents one item, e.g. "kdevelop stable for debian" """
    belongsToEntry = models.ForeignKey(ContentEntry)
    downloadlink = models.TextField()
    mimetype = models.CharField(max_length=200, default="application/tar")


