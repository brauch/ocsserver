from django.http import HttpResponse
from xml.etree import ElementTree as et
from models import Category, ContentEntry
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
import base64
from django.contrib.auth import authenticate, login
import os
import sys
from ocsserver.settings import *

def listCategories(request):
    """List all available categories"""
    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    et.SubElement(meta, "status").text = "ok"
    et.SubElement(meta, "statuscode").text = "100"
    et.SubElement(meta, "totalitems").text = str(Category.objects.count())
    data = et.SubElement(response, "data")
    categories = Category.objects.all()
    for category in categories:
        catElement = et.SubElement(data, "category")
        et.SubElement(catElement, "id").text = str(category.id)
        et.SubElement(catElement, "name").text = category.name
    return HttpResponse(et.tostring(response), status=200)

@csrf_exempt
def uploadFile(request, itemid):
    itemid = int(itemid)
    status = 100
    try:
        item = ContentEntry.objects.filter(id=itemid)[0]
    except Exception as e:
        # yeah whatever. The protocol doesn't specify an error
        # code for "no such entry", so we just send "upload error".
        # not that the KDE GHNS upload widget cared, it'll just
        # display "success" whatever the response is.
        status = 101
    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    if item.owner.id != request.user.id:
        status = 103
    et.SubElement(meta, "status").text = "ok" if status is 100 else "failed"
    et.SubElement(meta, "statuscode").text = str(status)
    if status is not 100:
        http = HttpResponse(et.tostring(response), status=200 if request.user.is_authenticated() else 401)
        if not request.user.is_authenticated(): # TODO is this necessay, or can we just return a 401?
            http['WWW-Authenticate'] = 'Basic realm="%s"' % ""
        return http
    # the FileField in the model handles saving the file to the
    # correct location.
    item.uploadedFile = request.FILES['localfile']
    item.save()
    return HttpResponse(et.tostring(response))

def licenses(request):
    """Should return a list of licenses; fow now, just return one license"""
    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    et.SubElement(meta, "status").text = "ok"
    et.SubElement(meta, "statuscode").text = "100"
    et.SubElement(meta, "totalitems").text = "1"
    data = et.SubElement(response, "data")
    gpl = et.SubElement(data, "license")
    et.SubElement(gpl, "id").text = "1"
    et.SubElement(gpl, "name").text = "GNU GPL"
    return HttpResponse(et.tostring(response))

def setupItemFromRequest(item, request):
    """Parse the POST data from the given request,
    and copy its properties to the item."""
    # I assume it's ok if two items with the same name exist? The spec doesn't tell.
    if request.POST.get("name") is None:
        raise ValueError("Name must not be None") # TODO catch those :D
    item.name = request.POST.get("name")
    item.description = request.POST.get("description", "")
    item.changelog = request.POST.get("changelog", "")
    item.version = request.POST.get("version", "0.1")
    item.license = request.POST.get("license", "unspecified")
    item.category = Category.objects.filter(id=int(request.POST.get("type")))[0]

@csrf_exempt
def edit(request, contentId):
    """Edit the selected content item"""
    contentId = int(contentId)
    contentItem = ContentEntry.objects.filter(id=contentId)[0]
    status = 100
    if contentItem.owner.id != request.user.id:
        status = 102
    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    et.SubElement(meta, "status").text = "ok"
    et.SubElement(meta, "statuscode").text = str(status)
    if status is 100:
        setupItemFromRequest(contentItem, request)
        contentItem.save()
    return HttpResponse(et.tostring(response), status=401 if status is 102 else 200)

@csrf_exempt # TODO is this necessary?
def add(request):
    """Add a content item"""
    status = 100
    if not request.user.is_authenticated():
        status = 102
    item = ContentEntry()
    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    if status is 100:
        try:
            setupItemFromRequest(item, request)
            item.created = datetime.now()
            item.changed = datetime.now()
            item.owner = request.user
            data = et.SubElement(response, "data")
            content = et.SubElement(data, "content")
        except Exception as e:
            status = 101
            message = str(e)
            et.SubElement(meta, "message").text = message
    et.SubElement(meta, "statuscode").text = str(status)
    et.SubElement(meta, "status").text = "ok" if status is 100 else "failed"
    if status is 100:
        item.save()
        et.SubElement(content, "id").text = str(item.id)
    http = HttpResponse(et.tostring(response), status=200 if status is not 102 else 401)
    if status is 102:
        http['WWW-Authenticate'] = 'Basic realm="%s"' % ""
    return http

def getData(request, contentId):
    try:
        contentId = int(contentId)
    except:
        return HttpResponse("content ID must be an integer", status=400)
    try:
        item = ContentEntry.objects.get(id=contentId)
        status = 100
    except:
        status = 101
    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    et.SubElement(meta, "status").text = "ok" if status is 100 else "failed"
    et.SubElement(meta, "statuscode").text = str(status)
    if status is not 100:
        # the requested item was not found
        return HttpResponse(et.tostring(response), status=200)
    data = et.SubElement(response, "data")
    content = et.SubElement(data, "content", details="full")
    et.SubElement(content, "id").text = str(item.id)
    et.SubElement(content, "name").text = item.name
    et.SubElement(content, "created").text = str(item.created)
    et.SubElement(content, "changed").text = str(item.changed)
    et.SubElement(content, "downloads").text = str(item.downloads)
    et.SubElement(content, "version").text = str(item.version)
    et.SubElement(content, "license").text = str(item.license)
    et.SubElement(content, "description").text = item.description
    et.SubElement(content, "summary").text = item.summary
    et.SubElement(content, "changelog").text = item.changelog
    return HttpResponse(et.tostring(response))

def download(request, contentId, itemId):
    status = 100
    mimetype = str()
    try:
        contentItem = ContentEntry.objects.get(id=int(contentId))
    except:
        status = 101
    else:
        try:
            # TODO: in what order are those items?
            # probably we need an extra int field to identify them reliably.
            item = contentItem.contentitem_set.all()[int(itemId)-1]
            mimetype = item.mimetype
        except:
            status = 100
            mimetype = "application/x-compressed-tar"
            # TODO change this as soon as multiple downloads are supported
            # status = 102

    contentItem.downloads += 1
    contentItem.save()

    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    et.SubElement(meta, "status").text = "ok" if status is 100 else "failed"
    et.SubElement(meta, "statuscode").text = str(status)
    if status is not 100:
        return HttpResponse(et.tostring(response))
    data = et.SubElement(response, "data")
    content = et.SubElement(data, "content", details="download")
    et.SubElement(content, "mimetype").text = mimetype
    et.SubElement(content, "downloadway").text = str(0)
    # TODO for now, only downloading the uploaded item is supported.
    et.SubElement(content, "downloadlink").text = contentItem.uploadedFile.url
    return HttpResponse(et.tostring(response))

def listItems(request):
    """Lists all items which match given search criteria"""
    # validate GET data
    try:
        inCategories = [int(item) for item in request.GET.get("categories").split("x") if item != ""]
    except Exception as e:
        return HttpResponse("categories must be a list of category IDs seperated by 'x': " + str(e), status=400)
    sortmode = request.GET.get("sortmode")
    if sortmode not in ["new", "alpha", "high", "down"]:
        return HttpResponse("invalid sort mode", status=400)
    license = request.GET.get("license")
    if license is None:
        license = ""
    search = request.GET.get("search")
    if search is None:
        search = ""
    try:
        page = int(request.GET.get("page"))
        pagesize = int(request.GET.get("pagesize"))
        assert pagesize > 0
    except:
        return HttpResponse("invalid page or page size", status=400)

    # filter out the relevant entries and sort them
    numEntries = ContentEntry.objects.count()
    entries = ContentEntry.objects.all()
    entries = entries.filter(name__contains=search)
    if len(inCategories) > 0:
        # assume that if no categories are provided, all categories should be shown
        entries = entries.filter(category__in=inCategories)
    sortColumn = {
        "new": "changed",
        "alpha": "name",
        "high": "rating",
        "down": "downloads"
    }[sortmode]
    entries = entries.order_by(sortColumn)
    # page should start at 0
    entries = entries[page*pagesize:(page+1)*pagesize]

    # construct response XML
    response = et.Element("ocs")
    meta = et.SubElement(response, "meta")
    et.SubElement(meta, "status").text = "ok"
    et.SubElement(meta, "statuscode").text = "100"
    et.SubElement(meta, "totalitems").text= str(numEntries)

    data = et.SubElement(response, "data")
    for entry in entries:
        entryElement = et.SubElement(data, "content", details="summary")
        et.SubElement(entryElement, "id").text = str(entry.id)
        et.SubElement(entryElement, "name").text = entry.name

    return HttpResponse(et.tostring(response), status=200)
